# xtoyr

Write French words.

## User documentation

This software isn't released yet.

## Developer documentation

> See [system requirements](https://tauri.app/v1/guides/getting-started/prerequisites#1-system-dependencies)

This project use [Tauri v1](https://tauri.app).

<!-- > Require `cargo install tauri-cli` -->

### See changes

```sh
cargo tauri dev
```

### Build for Windows

> Require `rustup target add x86_64-pc-windows-gnu`

<!-- > Require `cargo install cargo-xwin` -->

```sh
cargo tauri build --target x86_64-pc-windows-gnu
```

This creates executable at
`src-tauri/target/x86_64-pc-windows-gnu/release/xtoyr.exe`, you have to keep
the `WebView2Loader.dll` along with the executable.
